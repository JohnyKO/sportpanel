﻿namespace SportTable
{
    partial class MainPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelTime = new System.Windows.Forms.Label();
            this.labelScoreLeft = new System.Windows.Forms.Label();
            this.labelScoreRight = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 99.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTime.ForeColor = System.Drawing.Color.Yellow;
            this.labelTime.Location = new System.Drawing.Point(196, 5);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(398, 152);
            this.labelTime.TabIndex = 0;
            this.labelTime.Text = "00:00";
            // 
            // labelScoreLeft
            // 
            this.labelScoreLeft.AutoSize = true;
            this.labelScoreLeft.BackColor = System.Drawing.SystemColors.ControlText;
            this.labelScoreLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 99.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelScoreLeft.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelScoreLeft.Location = new System.Drawing.Point(12, 6);
            this.labelScoreLeft.Name = "labelScoreLeft";
            this.labelScoreLeft.Size = new System.Drawing.Size(139, 152);
            this.labelScoreLeft.TabIndex = 1;
            this.labelScoreLeft.Text = "0";
            // 
            // labelScoreRight
            // 
            this.labelScoreRight.AutoSize = true;
            this.labelScoreRight.BackColor = System.Drawing.SystemColors.ControlText;
            this.labelScoreRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 99.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelScoreRight.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelScoreRight.Location = new System.Drawing.Point(629, 5);
            this.labelScoreRight.Name = "labelScoreRight";
            this.labelScoreRight.Size = new System.Drawing.Size(139, 152);
            this.labelScoreRight.TabIndex = 2;
            this.labelScoreRight.Text = "0";
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // MainPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlText;
            this.ClientSize = new System.Drawing.Size(800, 166);
            this.Controls.Add(this.labelScoreRight);
            this.Controls.Add(this.labelScoreLeft);
            this.Controls.Add(this.labelTime);
            this.Name = "MainPanel";
            this.Text = "MainPanel";
            this.Load += new System.EventHandler(this.MainPanel_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Label labelScoreLeft;
        private System.Windows.Forms.Label labelScoreRight;
        private System.Windows.Forms.Timer timer;
    }
}