﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SportTable.Model;

namespace SportTable
{
    public partial class ControlPanel : Form
    {
        private MainPanel mainPanel = new MainPanel();

        public ControlPanel()
        {
            InitializeComponent();
        }

        private void ControlPanel_Load(object sender, EventArgs e)
        {
            mainPanel.Show();
            mainPanel.StopTimer();

            //Simple example for create new User with this pointer
            User user = new User();

            user.SetFirstName("Dima").SetLastName("Goncharuk").SetAge(14).Save();
            user.SetAge(19).SetLastName("Kostyuk").SetFirstName("Eugene").Save();
            //End example
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            mainPanel.StartTimer();
        }

        private void buttonAddLeft_Click(object sender, EventArgs e)
        {
            mainPanel.AddScoreLeft();
        }

        private void buttonAddRight_Click(object sender, EventArgs e)
        {
            mainPanel.AddScoreRight();
        }

        private void buttonRestart_Click(object sender, EventArgs e)
        {
            mainPanel.RestartTimer();
        }
    }
}
