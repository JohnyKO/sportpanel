﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SportTable
{
    public partial class MainPanel : Form
    {
        public MainPanel()
        {
            InitializeComponent();
        }

        public int ScoreLeft { get; private set; }
        public void AddScoreLeft()
        {
            ScoreLeft++;
            labelScoreLeft.Text = ScoreLeft.ToString();
        }
        public void GetScoreLeft()
        {
            ScoreLeft--;
            labelScoreLeft.Text = ScoreLeft.ToString();
        }


        public int ScoreRight { get; private set; }
        public void AddScoreRight()
        {
            ScoreRight++;
            labelScoreRight.Text = ScoreRight.ToString();
        }
        public void GetScoreRight()
        {
            ScoreRight--;
            labelScoreRight.Text = ScoreRight.ToString();
        }

        public int Seconds { get; private set; }

        public void StartTimer()
        {
            timer.Enabled = true;
        }
        public void StopTimer()
        {
            timer.Enabled = false;
        }
        public void RestartTimer()
        {
            Seconds = 0;
            labelTime.Text = "00:00";
            StopTimer();
        }

        void InitPanel()
        {
            ScoreLeft = ScoreRight = 0;
            labelScoreLeft.Text = ScoreLeft.ToString();
            labelScoreRight.Text = ScoreRight.ToString();

            Seconds = 0;
            StopTimer();
        }

        private int MAX_TIME = 2700;
        private void MainPanel_Load(object sender, EventArgs e)
        {
            InitPanel();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            Seconds++;
            if (Seconds > MAX_TIME)
            {
                StopTimer();
                return;
            }

            int Minutes = Seconds / 60;
            int CurrentSeconds = Seconds - (Minutes * 60);

            DateTime time = new DateTime(1, 1, 1, 1,
                Minutes, CurrentSeconds);

            String preMinute = "";
            String medium = ":";

            if (Minutes < 10)
                preMinute = "0";
            else
                preMinute = "";

            if (CurrentSeconds < 10)
                medium = ":0";
            else
                medium = ":";

            String resultShowTime = preMinute + time.Minute.ToString() + medium + time.Second.ToString();
            labelTime.Text = resultShowTime;
        }
    }
}
