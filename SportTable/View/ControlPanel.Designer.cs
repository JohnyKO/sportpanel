﻿namespace SportTable
{
    partial class ControlPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonRestart = new System.Windows.Forms.Button();
            this.buttonAddLeft = new System.Windows.Forms.Button();
            this.buttonAddRight = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonStart
            // 
            this.buttonStart.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonStart.Location = new System.Drawing.Point(247, 10);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(276, 57);
            this.buttonStart.TabIndex = 0;
            this.buttonStart.Text = "Start Time";
            this.buttonStart.UseVisualStyleBackColor = false;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // buttonRestart
            // 
            this.buttonRestart.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonRestart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRestart.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonRestart.Location = new System.Drawing.Point(247, 74);
            this.buttonRestart.Name = "buttonRestart";
            this.buttonRestart.Size = new System.Drawing.Size(276, 57);
            this.buttonRestart.TabIndex = 1;
            this.buttonRestart.Text = "Restart Time";
            this.buttonRestart.UseVisualStyleBackColor = false;
            this.buttonRestart.Click += new System.EventHandler(this.buttonRestart_Click);
            // 
            // buttonAddLeft
            // 
            this.buttonAddLeft.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonAddLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAddLeft.Location = new System.Drawing.Point(12, 10);
            this.buttonAddLeft.Name = "buttonAddLeft";
            this.buttonAddLeft.Size = new System.Drawing.Size(229, 121);
            this.buttonAddLeft.TabIndex = 2;
            this.buttonAddLeft.Text = "Add Left";
            this.buttonAddLeft.UseVisualStyleBackColor = false;
            this.buttonAddLeft.Click += new System.EventHandler(this.buttonAddLeft_Click);
            // 
            // buttonAddRight
            // 
            this.buttonAddRight.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonAddRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAddRight.Location = new System.Drawing.Point(529, 10);
            this.buttonAddRight.Name = "buttonAddRight";
            this.buttonAddRight.Size = new System.Drawing.Size(229, 121);
            this.buttonAddRight.TabIndex = 3;
            this.buttonAddRight.Text = "Add Right";
            this.buttonAddRight.UseVisualStyleBackColor = false;
            this.buttonAddRight.Click += new System.EventHandler(this.buttonAddRight_Click);
            // 
            // ControlPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 137);
            this.Controls.Add(this.buttonAddRight);
            this.Controls.Add(this.buttonAddLeft);
            this.Controls.Add(this.buttonRestart);
            this.Controls.Add(this.buttonStart);
            this.Name = "ControlPanel";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.ControlPanel_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonRestart;
        private System.Windows.Forms.Button buttonAddLeft;
        private System.Windows.Forms.Button buttonAddRight;
    }
}

