﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportTable.Model
{
    class User
    {
        private string firstName;
        private string lastName;
        private int age;

        public User()
        {
        }

        public User(string firstName, string lastName, int age)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.age = age;
        }

        public User SetFirstName(string firstName)
        {
            this.firstName = firstName;
            return this;
        }

        public User SetLastName(string lastName)
        {
            this.lastName = lastName;
            return this;
        }

        public User SetAge(int age)
        {
            this.age = age;
            return this;
        }

        public void Save()
        {
            //TODO somethining
        }
    }
}
